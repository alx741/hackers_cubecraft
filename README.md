Hackers Cubecraft
=================

This is a collection of funny **Cubecraft** figures of famous hackers, science
celebrities, personal idols, and other suggested relevant people.

These are used as desktop ornaments and are intended to be funny tributes.

All the figures are created using **GIMP** and you can find the _gimp_ source
files inside each `src` directory.

The images used for creating the figures are inside each `img` directory.

A thumbnail can probably be found inside each character directory.


Cubecraft list
==============

* [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds)
* [Richard Matthew Stallman](https://en.wikipedia.org/wiki/Richard_Stallman)
* [V (for vendetta)](https://en.wikipedia.org/wiki/V_for_Vendetta_(film))


Using
=====

1. Print the corresponding `cubecraft.pdf` file (A4 format)
2. Cut following the marks
3. Fold following the marks
4. Glue it!


Contributing
============

* Please send me pictures of your builds (to: alx741@riseup.net).

* `TODO` section lists the characters that are waiting to be created, the
difficult part is to find a good enough picture of the person (resolution, front
facing, illumination, funny [if possible], etc) so if you can find a good one
please send it to me.

* You can always suggest a new character with a new _issue_.

* If you want to create a new one by your self please ask first if the target
character will be approved through an _issue_, if so use the content in the
`template` directory and follow the directory structure of existing cube crafts.


TODO
====

* Dennis Ritchie
* Eric Raymond
* Ken Thompson


License
=======

This resources are licensed under the Creative Commons Attribution-ShareAlike
4.0 International license.

* By: Daniel Campoverde Carrión [Alx741]
* mailto: alx741@riseup.net
* GPG key id: 12622B78
* http: [Silly Bytes](http://www.silly-bytes.blogspot.com)
